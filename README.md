# WEB SERVICE EXPRESS (NODEJS)

Para ejecutar el WS es necesario configurar las variables
de conexión a la base de datos en `config/app.js`.

Luego ejecutar los comandos según se requiera.

#### CREATE
```sh
$ curl -d "username=cgimenez&password=casa1234&email=cgimenez@xmail.com" -X POST http://127.0.0.1:3000/user/add
```

#### DESACTIVATE
```sh
$ curl -d "username=cgimenez" -X POST http://127.0.0.1:3000/user/desactivate
```

#### ACTIVATE
```sh
$ curl -d "username=cgimenez" -X POST http://127.0.0.1:3000/user/activate
```

#### GET
```sh
$ curl -X GET http://127.0.0.1:3000/user/get?username=cgimenez
```