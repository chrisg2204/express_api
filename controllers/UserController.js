'use strict'

// Libs
const logger = require('../utils/LoggerUtil');
const Joi = require('joi');
const crypto = require('crypto');
// Utils
const response = require('../utils/ResponseUtil');
// Models
const models = require('../models/index');

class UserController {

	constructor() {}

	async addUser(req, res)  {
		logger.infoLevel('INFO - Ejecutando function addUser')

		let body = req.body;
		body.password = crypto.createHash('sha1').update(body.password).digest("hex");;

		const schema = Joi.object().keys({
			username: Joi.string().required(),
			password: Joi.string().required(),
			email: Joi.string().required(),
		});

		try {

			const objValid = await Joi.validate(body, schema);

			let user = models.user;
			let findOne = await user.findOne({"username": body.username});

			if (findOne) {
				response.sendResponse(res, 200, `Nombre de usuario ${body.username} ya existe.`, false);
				logger.errorLevel(`ERROR - Nombre de usuario ${body.username} ya existe.`);
			} else {
				let newUser = new models.user(objValid);
				let saveUser = await newUser.save();
				
				logger.infoLevel(`INFO - Procesamos request addUser | usuario: ${body.username} | password: ${body.password} | email:${body.email} `);
				response.sendResponse(res, 200, 'Usuario registrado con exito.', false);
			}

		} catch(err) {
			response.sendResponse(res, 500, {"status_code": 0}, true);
			logger.errorLevel(err);
			console.log(err);
		}
	}

	async desactivateUser(req, res)  {
		logger.infoLevel('INFO - Ejecutando function desactivateUser')

		let body = req.body;

		const schema = Joi.object().keys({
			username: Joi.string().required(),
		});

		try {

			const objValid = await Joi.validate(body, schema);

			let user = models.user;
			let findOne = await user.findOne({"username": body.username});

			if (findOne) {
				logger.infoLevel(`INFO - Procesamos request desactivateUser | usuario: ${body.username}`);
				findOne.state = false;
				findOne.save();

			}

			response.sendResponse(res, 200, {"status_code": 0}, false);

		} catch(err) {
			response.sendResponse(res, 500, err, true);
			logger.errorLevel(err);
			console.log(err);
		}
	}

	async activateUser(req, res)  {
		logger.infoLevel('INFO - Ejecutando function activateUser')

		let body = req.body;

		const schema = Joi.object().keys({
			username: Joi.string().required(),
		});

		try {

			const objValid = await Joi.validate(body, schema);

			let user = models.user;
			let findOne = await user.findOne({"username": body.username});

			if (findOne) {
				logger.infoLevel(`INFO - Procesamos request activateUser | usuario: ${body.username}`);
				findOne.state = true;
				findOne.save();

			}

			response.sendResponse(res, 200, {"status_code": 0}, false);

		} catch(err) {
			response.sendResponse(res, 500, err, true);
			logger.errorLevel(err);
			console.log(err);
		}
	}

	async getUser(req, res) {
		logger.infoLevel('INFO - Ejecutando function getUser')
		let queryParams = req.query;

		if ('username' in queryParams) {
			try {
			
				let user = models.user;
				let userData = await user.findOne({"username": queryParams.username});
				logger.infoLevel(`INFO - Procesamos request getUser | username: ${queryParams.username}`);
				
				response.sendResponse(res, 200, userData, false);

			} catch(err) {
				response.sendResponse(res, 500, err, true);
				logger.errorLevel(err);
				console.log(err);
			}
		} else {
			response.sendResponse(res, 404, "Falta parametro username", false);
		}
	}
	
}

module.exports = new UserController();