'use strict';

let db = require('./db');
let user = require('./user')(db);

let models = {};
    models.user = user;

module.exports = models;