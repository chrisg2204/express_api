'use strict';

let moment = require('moment');

module.exports = (db) => {
		let mongoose = db.mongoose,
			userSchema = new db.schema({
				username: {
                    type: String,
                    required: [true, 'name is required']
                },
                password: {
                    type: String,
                    required: [true, 'password is required']
                },
                email: {
                    type: String,
                    required: [true, 'email is required']
                },
                state: {
                    type: Boolean,
                    default: true,
                },
				date: {
					type: Date,
					required: [true, 'date is required'],
					default: moment()
				}
            });
            
        return mongoose.model('user', userSchema);
};