'use strict';

/**
 * Rutas disponibles juntos a sus
 * controladores.
 * @module src
 */

// Libs
const moment = require('moment');
// Config
const config = require('./config/app');
// Utils
const response = require('./utils/ResponseUtil');
const logger = require('./utils/LoggerUtil');
// Controllers
const userController = require('./controllers/UserController');

module.exports = (express) => {

	// log.setLevel(config.LOG_LEVEL) 
	let router = express.Router();

	/**
	* @apiDefine CustomContentType
	* @apiHeader {String="application/json"} Content-Type
	*/

   /**
	* @apiDefine successResponse
	* @apiSuccess {Integer} code     Código HTTP.
	* @apiSuccess {Object=null} error     Objeto de errores.
	* @apiSuccess {Object} data     Data de respuesta.
	*/

   /**
	* @apiDefine errorResponse
	* @apiError {Integer} code     Código HTTP.
	* @apiError {Object=null} data     Data de respuesta.
	* @apiError {Object} error     objeto de errores.
	*/

   /**
	* @apiIgnore Not necesary
	* @api {GET} /
	* @apiVersion 0.0.1
	* @apiName TestRoute
	* @apiGroup Test
	*/


	router.post('/user/add', (req, res) => {
		userController.addUser(req, res);
	});

	router.post('/user/desactivate', (req, res) => {
		userController.desactivateUser(req, res);
	});

	router.post('/user/activate', (req, res) => {
		userController.activateUser(req, res);
	});

	router.get('/user/get', (req, res) => {
		userController.getUser(req, res);
	});

	return router;
};